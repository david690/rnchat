import { StyleSheet } from "react-native";
import colors from "./colors";

export const darkMode = StyleSheet.create({
	name: "dark",
	container: {
		flex: 1,
		backgroundColor: colors.bigStone,
	},
	form: {
		flex: 1,
		justifyContent: 'center',
		marginHorizontal: 30,
	},
	homeContainer: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "#1a2b3f"
	},
	input: {
		backgroundColor: "#F6F7FB",
		height: 60,
		marginBottom: 20,
		fontSize: 16,
		borderRadius: 100,
		padding: 12,
	},
	pickerContainer: {
		height: 56,
		borderWidth: 1,
		width: "50%",
		borderColor: "white"
	},
	settingsContainer: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: colors.bigStone
	},
	textModoOscuro: {
		color: "white",
		fontWeight: "bold",
		fontSize: 20
	},
	touchableOpacity: {
		alignItems: "center",
		backgroundColor: colors.easternBlue,
		borderRadius: 100,
		height: 60,
		justifyContent: "center",
	},
	touchableOpacityText: {
		fontWeight: "bold",
		color: "#fff",
		fontSize: 18
	},
	textOrange: {
		color: "#ec5925",
		fontWeight: "600",
		fontSize: 14
	},
	titulo: {
		fontSize: 36,
		fontWeight: 'bold',
		color: "white",
		alignSelf: "center",
		paddingBottom: 24,
	}
});
