import React, {
	useState,
	useLayoutEffect,
	useCallback,
	useContext,
	useEffect
} from 'react';
import { GiftedChat } from 'react-native-gifted-chat';
import {
	collection,
	addDoc,
	orderBy,
	query,
	onSnapshot
} from 'firebase/firestore';
import { auth, database } from '../config/firebase';
import { useNavigation } from '@react-navigation/native';
import colors from '../styles/colors';
import { useTranslation } from 'react-i18next';
import themeContext from "../styles/themeContext";

import axios from "axios";
import { getPrefference } from '../config/UserPreferences';

export const Chat = () => {
	const globalTheme = useContext(themeContext);
	const [messages, setMessages] = useState([]);
	const [myMessage, setMyMessage] = useState(false);
	const navigation = useNavigation();
	const { t, i18n } = useTranslation();
	const [expoPushToken, setExpoPushToken] = useState('');

	useLayoutEffect(() => {
		getPrefference("expoPushToken").then(expoPushToken => {
			setExpoPushToken(expoPushToken);
		}).catch((error) => { console.log("Error al cargar el token.\n", error) });
	}, []);

	async function sendPushNotification(expoPushToken, mensaje) {
		const message = {
			to: expoPushToken,
			sound: Platform.OS !== "android" ? "default" : "default",
			title: 'Chat Local\'s Atlas',
			body: mensaje,
			data: { someData: 'goes here' },

		};
		await fetch('https://exp.host/--/api/v2/push/send', {
			method: 'POST',
			headers: {
				Accept: 'application/json',
				'Accept-encoding': 'gzip, deflate',
				'Content-Type': 'application/json',
			},
			body: JSON.stringify(message),
		});
	}

	const enviarMensaje = async (_id, createdAt, text, user) => {
		try {
			await axios.post("https://reactnativebackendarc.herokuapp.com/chats", {
				// await axios.post("http://192.168.1.6:5000/chats", {
				_id, createdAt, text, user
			}).then((response) => console.log(response.data));
		} catch (error) {
			console.log("error al ingresar un nuevo mensaje:", error)
		}
	}

	useLayoutEffect(() => {
		const collectionRef = collection(database, 'chats');
		const q = query(collectionRef, orderBy('createdAt', 'desc'));
		const unsubscribe = onSnapshot(q, querySnapshot => {
			console.log('querySnapshot unsusbscribe');
			setMessages(
				querySnapshot.docs.map(doc => ({
					_id: doc.data()._id,
					createdAt: doc.data().createdAt,
					text: doc.data().text,
					user: doc.data().user,
					image: doc.data().image,
					msg: doc.data().text
				}))
			);
		});
		return unsubscribe;
	}, []);

	const onSend = useCallback((messages = []) => {
		setMessages(previousMessages =>
			GiftedChat.append(previousMessages, messages)
		);
		const { _id, createdAt, text, user } = messages[0];
		axios.post(`https://app.nativenotify.com/api/indie/notification`, {
			subID: user._id,
			appId: 2708,
			appToken: 'dGqPDJPrjKz0FdZJTQv64m',
			title: 'Local\'s Atlas',
			message: text
		});
		// addDoc(collection(database, 'chats'), {
		// 	_id,
		// 	createdAt,
		// 	text,
		// 	user
		// });
		enviarMensaje(_id, createdAt, text, user);
	}, []);

	return (
		<GiftedChat
			messages={messages}
			showAvatarForEveryMessage={false}
			showUserAvatar={false}
			onSend={messages => onSend(messages)}
			messagesContainerStyle={{
				backgroundColor: (globalTheme.name == "dark") ? colors.bigStone : "white"
			}}
			textInputStyle={{
				backgroundColor: "white",
				borderRadius: 4,
			}}
			user={{
				_id: auth?.currentUser?.email,
				avatar: 'https://i.pravatar.cc/300',
				name: auth?.currentUser?.email.split("@")[0]
			}}
			renderUsernameOnMessage={true}
			placeholder={t("escribeMensaje")}
			isTyping={true}
			alwaysShowSend={true}
			renderAvatar={null}
			dateFormat="LLL"
		/>
	);
}

