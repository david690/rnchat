import React, { useContext, useEffect, useRef, useState } from "react";
import { View, TouchableOpacity, StyleSheet, Text } from "react-native";
import { useNavigation } from "@react-navigation/native";
import themeContext from "../styles/themeContext";
import * as Device from 'expo-device';
import * as Notifications from 'expo-notifications';
import { getPrefference, setPrefference } from "../config/UserPreferences";

Notifications.setNotificationHandler({
	handleNotification: async () => ({
		shouldShowAlert: true,
		shouldPlaySound: true,
		shouldSetBadge: false,
	}),
});

export const Home = () => {
	const [expoPushToken, setExpoPushToken] = useState('');
	const [notification, setNotification] = useState(false);
	const notificationListener = useRef();
	const responseListener = useRef();

	useEffect(() => {
		getPrefference("expoPushToken").then(expoPushToken => {
			if (expoPushToken === null) {
				registerForPushNotificationsAsync().then(token => setExpoPushToken(token));

				// This listener is fired whenever a notification is received while the app is foregrounded
				notificationListener.current = Notifications.addNotificationReceivedListener(notification => {
					setNotification(notification);
				});

				// This listener is fired whenever a user taps on or interacts with a notification (works when app is foregrounded, backgrounded, or killed)
				responseListener.current = Notifications.addNotificationResponseReceivedListener(response => {
					console.log(response);
				});

				return () => {
					Notifications.removeNotificationSubscription(notificationListener.current);
					Notifications.removeNotificationSubscription(responseListener.current);
				};
			} else {
				setExpoPushToken(expoPushToken);
			}
		}).catch((error) => { console.log("Error al cargar el token.\n", error) });

	}, []);

	async function sendPushNotification(expoPushToken, mensaje) {
		const message = {
			to: expoPushToken,
			sound: Platform.OS !== "android" ? "default" : "default",
			title: 'Chat Local\'s Atlas',
			body: mensaje,
			data: { someData: 'goes here' },

		};

		await fetch('https://exp.host/--/api/v2/push/send', {
			method: 'POST',
			headers: {
				Accept: 'application/json',
				'Accept-encoding': 'gzip, deflate',
				'Content-Type': 'application/json',
			},
			body: JSON.stringify(message),
		});
	}

	async function registerForPushNotificationsAsync() {
		let token;
		if (Device.isDevice) {
			const { status: existingStatus } = await Notifications.getPermissionsAsync();
			let finalStatus = existingStatus;
			if (existingStatus !== 'granted') {
				const { status } = await Notifications.requestPermissionsAsync();
				finalStatus = status;
			}
			if (finalStatus !== 'granted') {
				alert('Failed to get push token for push notification!');
				return;
			}
			token = (await Notifications.getExpoPushTokenAsync()).data;
		} else {
			alert('Must use physical device for Push Notifications');
		}

		if (Platform.OS === 'android') {
			Notifications.setNotificationChannelAsync('default', {
				name: 'default',
				importance: Notifications.AndroidImportance.MAX,
				vibrationPattern: [0, 250, 250, 250],
				lightColor: '#FF231F7C',
			});
		}
		setPrefference("expoPushToken", expoPushToken);
		return token;
	}

	const globalTheme = useContext(themeContext);
	const navigation = useNavigation();
	return (
		<View style={globalTheme.homeContainer}>
			<TouchableOpacity
				onPress={() => navigation.navigate("Chat")}
				style={[globalTheme.touchableOpacity, { width: "50%" }]}
			>
				<Text style={globalTheme.touchableOpacityText}>Chat</Text>
			</TouchableOpacity>
		</View>
	);
};
