import React, { useContext, useEffect, useState } from "react";
import { Text, View, TextInput, Image, SafeAreaView, TouchableOpacity, Alert } from "react-native";
import { signInWithEmailAndPassword } from "firebase/auth";
import { auth } from "../config/firebase";
import { useTranslation } from 'react-i18next';
import { getPrefference } from "../config/UserPreferences";
import themeContext from "../styles/themeContext";
import axios from "axios";
import { registerIndieID } from 'native-notify';

export default function Login({ navigation }) {

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const { t, i18n } = useTranslation();

	useEffect(() => {
		getPrefference("idioma").then(idioma => {
			i18n.changeLanguage(idioma)
				.then(() => console.log("val", idioma))
				.catch(error => {
					console.log("Error al actualizar el idioma:", error);
				});
		});
	}, []);

	const globalTheme = useContext(themeContext);


	const setAuth = () => {
		axios.post("http://192.168.1.6:5000/auth").then(
			res => {
				auth = res.data.auth;
			}
		).catch(error => {
			Alert.alert("Error", error);
		});
	}

	const onHandleLogin = () => {
		if (email !== "" && password !== "") {
			signInWithEmailAndPassword(auth, email, password)
				.then(() => { console.log("Login success"), registerIndieID(email, 2708, 'dGqPDJPrjKz0FdZJTQv64m'); })
				.catch((err) => Alert.alert("Login error", err.message));
		}
	};

	const iniciarSesion = () => {
		axios.post("http://192.168.1.6:5000/login", { email: email, password: password })
			.then((response) => {
				if (response.data.message == "error") {
					Alert.alert(response.data.errorMessage.name, response.data.errorMessage.code);
				} else {
					console.log("Inicio de sesión exitoso.");
				}
			});
	}

	return (
		<View style={globalTheme.container}>
			<SafeAreaView style={globalTheme.form}>
				<Text style={globalTheme.titulo}>{t("iniciarSesion")}</Text>

				<Image source={require('../../assets/logo.png')} style={{ alignSelf: "center", marginVertical: 20 }} />

				<Text style={[globalTheme.titulo, { fontSize: 24 }]}>Local's Atlas</Text>

				<TextInput
					style={globalTheme.input}
					placeholder={t("correoElectronico")}
					autoCapitalize="none"
					textContentType="emailAddress"
					value={email}
					onChangeText={(text) => setEmail(text)}
				/>
				<TextInput
					style={globalTheme.input}
					placeholder={t("contraseña")}
					autoCapitalize="none"
					autoCorrect={false}
					secureTextEntry={true}
					textContentType="password"
					value={password}
					onChangeText={(text) => setPassword(text)}
				/>

				<TouchableOpacity style={globalTheme.touchableOpacity} onPress={onHandleLogin}>
					<Text style={globalTheme.touchableOpacityText}>{t("iniciarSesion")}</Text>
				</TouchableOpacity>

				<View style={{ marginTop: 20, flexDirection: 'row', alignItems: 'center', alignSelf: 'center' }}>
					{/* <Text style={{ color: 'gray', fontWeight: '600', fontSize: 14 }}>Don't have an account? </Text> */}
					<TouchableOpacity onPress={() => navigation.navigate("Signup")}>
						<Text style={globalTheme.textOrange}>{t("crearCuenta")}</Text>
					</TouchableOpacity>
				</View>
			</SafeAreaView>
		</View>
	);
}

// const styles = StyleSheet.create({
// 	container: {
// 		flex: 1,
// 		backgroundColor: "#fff",
// 	},
// 	title: {
// 		fontSize: 36,
// 		fontWeight: 'bold',
// 		color: "orange",
// 		alignSelf: "center",
// 		paddingBottom: 24,
// 	},
// 	input: {
// 		backgroundColor: "#F6F7FB",
// 		height: 58,
// 		marginBottom: 20,
// 		fontSize: 16,
// 		borderRadius: 10,
// 		padding: 12,
// 	},
// 	backImage: {
// 		width: "100%",
// 		height: 340,
// 		position: "absolute",
// 		top: 0,
// 		resizeMode: 'cover',
// 	},
// 	whiteSheet: {
// 		width: '100%',
// 		height: '75%',
// 		position: "absolute",
// 		bottom: 0,
// 		backgroundColor: '#fff',
// 		borderTopLeftRadius: 60,
// 	},
// 	form: {
// 		flex: 1,
// 		justifyContent: 'center',
// 		marginHorizontal: 30,
// 	},
// 	button: {
// 		backgroundColor: '#f57c00',
// 		height: 58,
// 		borderRadius: 10,
// 		justifyContent: 'center',
// 		alignItems: 'center',
// 		marginTop: 40,
// 	},
// });