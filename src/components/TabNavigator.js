import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import Icon from 'react-native-vector-icons/Ionicons';

import colors from "../styles/colors";
import { Home } from "../screens/Home";
import { Chat } from "../screens/Chat";
import { Settings } from "../screens/Settings";

import themeContext from "../styles/themeContext";
import { useTranslation } from 'react-i18next';
import { useContext } from "react";

const tab = createBottomTabNavigator();
const StackNavigator = createNativeStackNavigator();

const Stack = () => {
	const globalTheme = useContext(themeContext);
	return (
		<StackNavigator.Navigator initialRouteName="HomeScreen">
			<StackNavigator.Screen
				name="HomeScreen"
				component={Home}
				options={{
					headerShown: false
				}}
			/>
			<StackNavigator.Screen
				name="Chat"
				component={Chat}
				options={{
					headerStyle: { backgroundColor: (globalTheme.name == "dark") ? colors.bigStone : "white" },
					headerTintColor: (globalTheme.name == "dark") ? "white" : colors.bigStone
				}}
			/>
		</StackNavigator.Navigator>
	);
}

function TabList() {
	const { t, i18n } = useTranslation();
	const globalTheme = useContext(themeContext);

	return (
		<tab.Navigator
			initialRouteName="Home"
			screenOptions={{
				tabBarActiveTintColor: colors.easternBlue,
				tabBarStyle: {
					backgroundColor: (globalTheme.name == "dark") ? colors.bigStone : "white"
				}
			}}
			barStyle={{ color: "yellow" }}
		>
			<tab.Screen
				name="Home"
				component={Stack}
				options={{
					tabBarLabel: t("chats"),
					tabBarIcon: ({ color, size }) => (
						<Icon name="chatbubbles" size={size} color={color} />
					),
					tabBarBadge: 2,
					headerShown: false
				}}
			/>
			<tab.Screen
				name="Settings"
				component={Settings}
				options={{
					tabBarLabel: t("configuracion"),
					tabBarIcon: ({ color, size }) => (
						<Icon name="settings" size={size} color={color} />
					),
					headerShown: false
				}}
			/>
		</tab.Navigator>
	);
}

export const TabNavigator = () => {
	return (
		<TabList />
	);
}