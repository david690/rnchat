import React, { useState, createContext, useContext, useEffect } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { View, ActivityIndicator, StatusBar, LogBox } from 'react-native';
import { onAuthStateChanged } from 'firebase/auth';
import { auth } from "./src/config/firebase";
import Login from './src/screens/Login';
import Signup from './src/screens/Signup';
import { TabNavigator } from './src/components/TabNavigator';

import { getPrefference } from './src/config/UserPreferences';

import themeContext from './src/styles/themeContext';
import { lightMode } from './src/styles/lightMode';
import { darkMode } from './src/styles/darkMode';
import colors from './src/styles/colors';
import registerNNPushToken, { registerIndieID } from 'native-notify';
import axios from 'axios';

const Stack = createStackNavigator();
export const AuthenticatedUserContext = createContext({});

const AuthenticatedUserProvider = ({ children }) => {
	const [user, setUser] = useState(null);
	return (
		<AuthenticatedUserContext.Provider value={{ user, setUser }}>
			{children}
		</AuthenticatedUserContext.Provider>
	);
};

function AuthStack() {
	return (
		<Stack.Navigator screenOptions={{ headerShown: false }}>
			<Stack.Screen name='Login' component={Login} />
			<Stack.Screen name='Signup' component={Signup} />
		</Stack.Navigator>
	);
}

function RootNavigator() {
	const { user, setUser } = useContext(AuthenticatedUserContext);
	const [isLoading, setIsLoading] = useState(true);
	useEffect(() => {
		// onAuthStateChanged returns an unsubscriber
		const unsubscribeAuth = onAuthStateChanged(
			auth,
			async authenticatedUser => {
				authenticatedUser ? setUser(authenticatedUser) : setUser(null);
				setIsLoading(false);
			}
		);
		// unsubscribe auth listener on unmount
		return unsubscribeAuth;
	}, [user]);
	if (isLoading) {
		return (
			<View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
				<ActivityIndicator size='large' />
			</View>
		);
	}

	return (
		<NavigationContainer>
			{user ? <TabNavigator /> : <AuthStack />}
		</NavigationContainer>
	);
}

export default function App() {
	registerNNPushToken(2708, 'dGqPDJPrjKz0FdZJTQv64m');

	const [tema, setTema] = useState("dark");
	useEffect(() => {
		getPrefference("tema").then(tema => {
			setTema(tema);
		}).catch((error) => { console.log("Error al cargar el tema, se ha asignado dark por defecto.\n", error) });
		LogBox.ignoreLogs(['Warning: ...']);
		LogBox.ignoreAllLogs();
	}, []);


	return (
		<themeContext.Provider value={tema == "dark" ? darkMode : lightMode}>
			<StatusBar
				barStyle={(tema == "dark") ? 'light-content' : "dark-content"}
				backgroundColor={(tema == "dark") ? colors.bigStone : "white"}
			/>
			<AuthenticatedUserProvider>
				<RootNavigator />
			</AuthenticatedUserProvider>
		</themeContext.Provider>
	);
}